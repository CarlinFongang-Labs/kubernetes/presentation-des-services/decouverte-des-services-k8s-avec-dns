# 4. Découverte des services K8s avec DNS

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Découverte des services Kubernetes avec DNS

Dans cette leçon, nous allons parler de la découverte des services Kubernetes avec DNS. Voici les points que nous allons couvrir :

1. Noms DNS des services
2. Relation entre les services DNS et les namespaces
3. Démonstration pratique

# Noms DNS des services

Le système DNS de Kubernetes assigne des noms DNS aux services, ce qui permet aux applications du cluster de localiser et de communiquer facilement avec eux. Le nom de domaine complet d'un service a le format suivant :

```bash
nom-du-service.nom-du-namespace.svc.domaine-du-cluster.exemple
```

Le domaine du cluster par défaut dans notre cluster Kubernetes est `cluster.local`. Donc, après `svc.`, ce sera généralement `cluster.local`.

# Relation entre les services DNS et les namespaces

Le nom de domaine complet vu précédemment peut être utilisé pour atteindre le service depuis n'importe quel namespace dans le cluster. Cela signifie que si un pod est dans un namespace complètement différent de celui du service, il peut toujours communiquer avec ce service en utilisant le nom de domaine complet. Cependant, les pods dans le même namespace peuvent utiliser une forme plus courte, qui consiste simplement en le nom du service.

```bash
my-service.my-namespace.svc.cluster.local
```

# Démonstration pratique

Voyons à quoi cela ressemble dans notre cluster Kubernetes.

Je suis connecté à mon nœud de plan de contrôle Kubernetes et je vais exécuter la commande suivante pour obtenir les informations du service `svc-clusterip` :

```bash
kubectl get service svc-clusterip
```

Ceci est un service que j'ai créé dans une leçon précédente. Si vous suivez ce cours, vous voudrez peut-être revenir sur certaines des leçons antérieures pour vous assurer que vous avez les objets créés plus tôt dont nous aurons besoin pour cette démonstration.

# Vérification du service et du pod

Obtenons également les pods disponibles :

```bash
kubectl get pods
```

J'ai aussi ce `pod-service`, un pod busybox que j'ai créé dans une leçon précédente. Je vais utiliser ce pod pour exécuter des commandes et interagir avec le service `svc-clusterip`.

# Utilisation de nslookup

Exécutons une commande `nslookup` dans le pod busybox pour vérifier les enregistrements DNS :

```bash
kubectl exec -it pod-service-test -- nslookup <adresse-IP-du-service>
```

Cela nous montre le nom de domaine complet de notre service :

```
svc-clusterip.default.svc.cluster.local
```

# Communication avec le service

Nous pouvons utiliser cette adresse pour communiquer avec le service. Exécutons une commande curl en utilisant l'adresse IP du service :

```bash
kubectl exec -it pod-service-test -- curl <adresse-IP-du-service>
```

Nous obtenons la page d'accueil nginx. Nous pouvons également utiliser simplement le nom du service, car le pod et le service sont dans le même namespace :

```bash
kubectl exec -it pod-service-test -- curl svc-clusterip
```

Cela fonctionne aussi. Enfin, utilisons le nom de domaine complet :

```bash
kubectl exec -it pod-service-test -- curl svc-clusterip.default.svc.cluster.local
```

Nous obtenons encore une fois la page d'accueil nginx.

# Communication entre namespaces

Créons un nouveau namespace et un pod dans ce namespace pour voir comment cela fonctionne entre namespaces :

```bash
kubectl create namespace new-namespace
```

Créons un fichier de définition pour un nouveau pod :

```bash
nano pod-service-test-new-namespace.yml
```

Ajoutons-y le contenu suivant :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-service-test-new-namespace
  namespace: new-namespace
spec:
  containers:
  - name: busybox
    image: radial/busyboxplus:curl
    command: ['sh', '-c', 'while true; do sleep 10; done']
```

Créons ce pod :

```bash
kubectl create -f pod-service-test-new-namespace.yml
```

Essayons maintenant de communiquer avec le service depuis ce nouveau namespace en utilisant le nom du service :

```bash
kubectl exec -it -n new-namespace pod-service-test-new-namespace -- curl svc-clusterip
```

Cela ne fonctionne pas. Mais si nous utilisons le nom de domaine complet :

```bash
kubectl exec -it -n new-namespace pod-service-test-new-namespace -- curl svc-clusterip.default.svc.cluster.local
```

Cela fonctionne.

# Conclusion

Dans cette leçon, nous avons parlé des noms DNS des services, de la relation entre les services DNS et les namespaces, et nous avons fait une démonstration pratique des différentes manières de communiquer avec les services à travers les namespaces ou au sein du même namespace en utilisant le DNS de Kubernetes. C'est tout pour cette leçon. À la prochaine !


# Reférences



https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/